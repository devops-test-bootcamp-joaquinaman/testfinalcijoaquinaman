import static org.junit.jupiter.api.Assertions.*;

class MainTest {

    @org.junit.jupiter.api.Test
    void checkIfIsVowelAndItIsVowel() {

        char ch1 = 'a';
        int sum = 0;

        switch (ch1) {
            case 'a':
            case 'e':
            case 'i':
            case 'o':
            case 'u':
                sum++;
                break;
        }
        assertTrue(sum==1);
    }
        @org.junit.jupiter.api.Test
        void checkIfIsVowelAndItIsNotVowel() {

            char ch2 = 'z';
            int sum = 0;

            switch (ch2) {
                case 'a':
                case 'e':
                case 'i':
                case 'o':
                case 'u':
                    sum ++;
                    break;
            }
            assertTrue(sum==0);
        }
}